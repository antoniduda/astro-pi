# Astro Pi
This was our entry to the Astro Pi competition.  

We were planning to measure what kind of influence does Sun have on magnetic
field detected by magnetometer.  

To achieve this we used vector maths in order to calculate angle between the
magnetic field B and the direction of ISS.  

# Contributors to the project
- Antoni Duda
- Maciek Chamera
- Feliks Kłos
- Szymon Sms
